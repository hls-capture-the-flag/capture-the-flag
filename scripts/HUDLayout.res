﻿resource/hudlayout.res
{
	hudhealth
	{
		fieldname	"HudHealth"
		xpos	"16"
		ypos	"432"
		wide	"102"
		tall	"36"
		visible	"1"
		enabled	"1"
		paintbackgroundtype	"2"
		text_xpos	"8"
		text_ypos	"20"
		digit_xpos	"50"
		digit_ypos	"2"
	}
	targetid
	{
		fieldname	"TargetID"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	teamdisplay
	{
		fieldname	"TeamDisplay"
		visible	"0"
		enabled	"1"
		xpos	"16"
		ypos	"415"
		wide	"200"
		tall	"60"
		text_xpos	"8"
		text_ypos	"4"
	}
	hudvoiceselfstatus
	{
		fieldname	"HudVoiceSelfStatus"
		visible	"1"
		enabled	"1"
		xpos	"r43"
		ypos	"355"
		wide	"24"
		tall	"24"
	}
	hudvoicestatus
	{
		fieldname	"HudVoiceStatus"
		visible	"1"
		enabled	"1"
		xpos	"r145"
		ypos	"0"
		wide	"145"
		tall	"400"
		item_wide	"135"
		show_avatar	"0"
		show_dead_icon	"1"
		dead_xpos	"1"
		dead_ypos	"0"
		dead_wide	"16"
		dead_tall	"16"
		show_voice_icon	"1"
		icon_ypos	"0"
		icon_xpos	"15"
		icon_tall	"16"
		icon_wide	"16"
		text_xpos	"33"
	}
	hudsuit
	{
		fieldname	"HudSuit"
		xpos	"140"
		ypos	"432"
		wide	"108"
		tall	"36"
		visible	"1"
		enabled	"1"
		paintbackgroundtype	"2"
		text_xpos	"8"
		text_ypos	"20"
		digit_xpos	"50"
		digit_ypos	"2"
	}
	hudammo
	{
		fieldname	"HudAmmo"
		xpos	"r150"
		ypos	"432"
		wide	"136"
		tall	"36"
		visible	"1"
		enabled	"1"
		paintbackgroundtype	"2"
		text_xpos	"8"
		text_ypos	"20"
		digit_xpos	"44"
		digit_ypos	"2"
		digit2_xpos	"98"
		digit2_ypos	"16"
	}
	hudammosecondary
	{
		fieldname	"HudAmmoSecondary"
		xpos	"r76"
		ypos	"432"
		wide	"60"
		tall	"36"
		visible	"1"
		enabled	"1"
		paintbackgroundtype	"2"
		digit_xpos	"10"
		digit_ypos	"2"
	}
	hudsuitpower
	{
		fieldname	"HudSuitPower"
		visible	"1"
		enabled	"1"
		xpos	"16"
		ypos	"396"
		wide	"102"
		tall	"26"
		auxpowerlowcolor	"255 0 0 220"
		auxpowerhighcolor	"255 220 0 220"
		auxpowerdisabledalpha	"70"
		barinsetx	"8"
		barinsety	"15"
		barwidth	"92"
		barheight	"4"
		barchunkwidth	"6"
		barchunkgap	"3"
		text_xpos	"8"
		text_ypos	"4"
		text2_xpos	"8"
		text2_ypos	"22"
		text2_gap	"10"
		paintbackgroundtype	"2"
	}
	hudflashlight
	{
		fieldname	"HudFlashlight"
		visible	"0"
		enabled	"1"
		xpos	"16"
		ypos	"370"
		wide	"102"
		tall	"20"
		text_xpos	"8"
		text_ypos	"6"
		textcolor	"255 170 0 220"
		paintbackgroundtype	"2"
	}
	huddamageindicator
	{
		fieldname	"HudDamageIndicator"
		visible	"1"
		enabled	"1"
		dmgcolorleft	"255 0 0 0"
		dmgcolorright	"255 0 0 0"
		dmg_xpos	"30"
		dmg_ypos	"100"
		dmg_wide	"36"
		dmg_tall1	"240"
		dmg_tall2	"200"
	}
	hudzoom
	{
		fieldname	"HudZoom"
		visible	"1"
		enabled	"1"
		circle1radius	"66"
		circle2radius	"74"
		dashgap	"16"
		dashheight	"4"
		borderthickness	"88"
	}
	hudweaponselection
	{
		fieldname	"HudWeaponSelection"
		ypos	"16"
		visible	"1"
		enabled	"1"
		smallboxsize	"32"
		largeboxwide	"112"
		largeboxtall	"80"
		boxgap	"8"
		selectionnumberxpos	"4"
		selectionnumberypos	"4"
		selectiongrowtime	"0.4"
		textypos	"64"
	}
	hudcrosshair
	{
		fieldname	"HudCrosshair"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	huddeathnotice
	{
		fieldname	"HudDeathNotice"
		visible	"1"
		enabled	"1"
		xpos	"r640"
		ypos	"12"
		wide	"628"
		tall	"468"
		maxdeathnotices	"4"
		lineheight	"22"
		rightjustify	"1"
		textfont	"Default"
	}
	hudvehicle
	{
		fieldname	"HudVehicle"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	scorepanel
	{
		fieldname	"ScorePanel"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	hudtrain
	{
		fieldname	"HudTrain"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	hudmotd
	{
		fieldname	"HudMOTD"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	hudmessage
	{
		fieldname	"HudMessage"
		visible	"1"
		enabled	"1"
		wide	"f0"
		tall	"480"
	}
	hudmenu
	{
		fieldname	"HudMenu"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	hudclosecaption
	{
		fieldname	"HudCloseCaption"
		visible	"1"
		enabled	"1"
		xpos	"c-250"
		ypos	"276"
		wide	"500"
		tall	"136"
		bgalpha	"128"
		growtime	"0.25"
		itemhiddentime	"0.2"
		itemfadeintime	"0.15"
		itemfadeouttime	"0.3"
	}
	hudhistoryresource
	{
		fieldname	"HudHistoryResource"
		visible	"1"
		enabled	"1"
		xpos	"r252"
		ypos	"40"
		wide	"248"
		tall	"320"
		history_gap	"56"
		icon_inset	"28"
		text_inset	"26"
		numberfont	"HudNumbersSmall"
	}
	hudgeiger
	{
		fieldname	"HudGeiger"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	hudquickinfo
	{
		fieldname	"HUDQuickInfo"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	hudweapon
	{
		fieldname	"HudWeapon"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	hudanimationinfo
	{
		fieldname	"HudAnimationInfo"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	hudpredictiondump
	{
		fieldname	"HudPredictionDump"
		visible	"1"
		enabled	"1"
		wide	"640"
		tall	"480"
	}
	hudhintdisplay
	{
		fieldname	"HudHintDisplay"
		visible	"0"
		enabled	"1"
		xpos	"r120"
		ypos	"r340"
		wide	"100"
		tall	"200"
		text_xpos	"8"
		text_ypos	"8"
		text_xgap	"8"
		text_ygap	"8"
		textcolor	"255 170 0 220"
		paintbackgroundtype	"2"
	}
	hudsquadstatus
	{
		fieldname	"HudSquadStatus"
		visible	"1"
		enabled	"1"
		xpos	"r120"
		ypos	"380"
		wide	"104"
		tall	"46"
		text_xpos	"8"
		text_ypos	"34"
		squadiconcolor	"255 220 0 160"
		iconinsetx	"8"
		iconinsety	"0"
		icongap	"24"
		paintbackgroundtype	"2"
	}
	hudpoisondamageindicator
	{
		fieldname	"HudPoisonDamageIndicator"
		visible	"0"
		enabled	"1"
		xpos	"16"
		ypos	"346"
		wide	"136"
		tall	"38"
		text_xpos	"8"
		text_ypos	"8"
		text_ygap	"14"
		textcolor	"255 170 0 220"
		paintbackgroundtype	"2"
	}
	hudcredits
	{
		fieldname	"HudCredits"
		textfont	"Default"
		visible	"1"
		xpos	"0"
		ypos	"0"
		wide	"640"
		tall	"480"
		textcolor	"255 255 255 192"
	}
	hudchat
	{
		controlname	"EditablePanel"
		fieldname	"HudChat"
		visible	"1"
		enabled	"1"
		xpos	"10"
		ypos	"275"
		wide	"320"
		tall	"120"
		paintbackgroundtype	"2"
	}
	achievementnotificationpanel
	{
		fieldname	"AchievementNotificationPanel"
		visible	"0"
		enabled	"0"
	}
	hudhintkeydisplay
	{
		fieldname	"HudHintKeyDisplay"
		visible	"0"
		enabled	"0"
	}
	hudautoaim
	{
		fieldname	"HUDAutoAim"
		visible	"0"
		enabled	"0"
	}
	hudhdrdemo
	{
		fieldname	"HudHDRDemo"
		visible	"0"
		enabled	"0"
	}
	hudcommentary
	{
		fieldname	"HudCommentary"
		visible	"0"
		enabled	"0"
	}
	chudvote
	{
		fieldname	"CHudVote"
		visible	"0"
		enabled	"0"
	}
}
