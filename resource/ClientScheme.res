﻿scheme
{
	colors
	{
		orange	"255 176 0 255"
		orangedim	"255 176 0 120"
		lightorange	"188 112 0 128"
		red	"192 28 0 140"
		black	"0 0 0 196"
		transparentblack	"0 0 0 196"
		transparentlightblack	"0 0 0 90"
		blank	"0 0 0 0"
		fortesting	"255 0 0 32"
		fortesting_magenta	"255 0 255 255"
		fortesting_magentadim	"255 0 255 120"
	}
	basesettings
	{
		fgcolor	"151 72 6 255"
		bgcolor	"0 0 0 255"
		panel.fgcolor	"255 220 0 100"
		panel.bgcolor	"0 0 0 76"
		brightfg	"255 192 0 255"
		damagedbg	"180 0 0 200"
		damagedfg	"180 0 0 230"
		brightdamagedfg	"255 0 0 255"
		selectionnumberfg	"255 220 0 255"
		selectiontextfg	"255 220 0 255"
		selectionemptyboxbg	"0 0 0 80"
		selectionboxbg	"0 0 0 80"
		selectionselectedboxbg	"0 0 0 80"
		zoomreticlecolor	"255 220 0 255"
		yellowish	"255 160 0 255"
		normal	"255 208 64 255"
		caution	"192 0 0 255"
		main.title1.x	"76"
		main.title1.y	"184"
		main.title1.color	"255 255 255 255"
		main.title2.x	"315"
		main.title2.y	"222"
		main.title2.color	"255 255 255 180"
		main.menu.x	"76"
		main.menu.y	"240"
		main.bottomborder	"32"
		border.bright	"LightOrange"
		border.dark	"LightOrange"
		border.selection	"Blank"
		button.textcolor	"Orange"
		button.bgcolor	"Blank"
		button.armedtextcolor	"Orange"
		button.armedbgcolor	"Red"
		button.depressedtextcolor	"Orange"
		button.depressedbgcolor	"Red"
		checkbutton.textcolor	"Orange"
		checkbutton.selectedtextcolor	"Orange"
		checkbutton.bgcolor	"TransparentBlack"
		checkbutton.border1	"Border.Dark"
		checkbutton.border2	"Border.Bright"
		checkbutton.check	"Orange"
		comboboxbutton.arrowcolor	"Orange"
		comboboxbutton.armedarrowcolor	"Orange"
		comboboxbutton.bgcolor	"TransparentBlack"
		comboboxbutton.disabledbgcolor	"Blank"
		frame.bgcolor	"TransparentBlack"
		frame.outoffocusbgcolor	"TransparentBlack"
		frame.focustransitioneffecttime	"0.0"
		frame.transitioneffecttime	"0.0"
		frame.autosnaprange	"0"
		framegrip.color1	"Blank"
		framegrip.color2	"Blank"
		frametitlebutton.fgcolor	"Blank"
		frametitlebutton.bgcolor	"Blank"
		frametitlebutton.disabledfgcolor	"Blank"
		frametitlebutton.disabledbgcolor	"Blank"
		framesystembutton.fgcolor	"Blank"
		framesystembutton.bgcolor	"Blank"
		framesystembutton.icon	""
		framesystembutton.disabledicon	""
		frametitlebar.textcolor	"Orange"
		frametitlebar.bgcolor	"Blank"
		frametitlebar.disabledtextcolor	"Orange"
		frametitlebar.disabledbgcolor	"Blank"
		graphpanel.fgcolor	"Orange"
		graphpanel.bgcolor	"TransparentBlack"
		label.textdullcolor	"Orange"
		label.textcolor	"Orange"
		label.textbrightcolor	"Orange"
		label.selectedtextcolor	"Orange"
		label.bgcolor	"Blank"
		label.disabledfgcolor1	"Blank"
		label.disabledfgcolor2	"LightOrange"
		listpanel.textcolor	"Orange"
		listpanel.bgcolor	"TransparentBlack"
		listpanel.selectedtextcolor	"Black"
		listpanel.selectedbgcolor	"Red"
		listpanel.selectedoutoffocusbgcolor	"Red"
		listpanel.emptylistinfotextcolor	"Orange"
		menu.textcolor	"Orange"
		menu.bgcolor	"TransparentBlack"
		menu.armedtextcolor	"Orange"
		menu.armedbgcolor	"Red"
		menu.textinset	"6"
		chat.typingtext	"Orange"
		panel.fgcolor	"OrangeDim"
		panel.bgcolor	"blank"
		progressbar.fgcolor	"Orange"
		progressbar.bgcolor	"TransparentBlack"
		propertysheet.textcolor	"Orange"
		propertysheet.selectedtextcolor	"Orange"
		propertysheet.transitioneffecttime	"0.25"
		radiobutton.textcolor	"Orange"
		radiobutton.selectedtextcolor	"Orange"
		richtext.textcolor	"Orange"
		richtext.bgcolor	"Blank"
		richtext.selectedtextcolor	"Orange"
		richtext.selectedbgcolor	"Blank"
		scrollbarbutton.fgcolor	"Orange"
		scrollbarbutton.bgcolor	"Blank"
		scrollbarbutton.armedfgcolor	"Orange"
		scrollbarbutton.armedbgcolor	"Blank"
		scrollbarbutton.depressedfgcolor	"Orange"
		scrollbarbutton.depressedbgcolor	"Blank"
		scrollbarslider.fgcolor	"Blank"
		scrollbarslider.bgcolor	"Blank"
		sectionedlistpanel.headertextcolor	"Orange"
		sectionedlistpanel.headerbgcolor	"Blank"
		sectionedlistpanel.dividercolor	"Black"
		sectionedlistpanel.textcolor	"Orange"
		sectionedlistpanel.brighttextcolor	"Orange"
		sectionedlistpanel.bgcolor	"TransparentLightBlack"
		sectionedlistpanel.selectedtextcolor	"Black"
		sectionedlistpanel.selectedbgcolor	"Red"
		sectionedlistpanel.outoffocusselectedtextcolor	"Black"
		sectionedlistpanel.outoffocusselectedbgcolor	"255 255 255 32"
		slider.nobcolor	"108 108 108 255"
		slider.textcolor	"127 140 127 255"
		slider.trackcolor	"31 31 31 255"
		slider.disabledtextcolor1	"117 117 117 255"
		slider.disabledtextcolor2	"30 30 30 255"
		textentry.textcolor	"Orange"
		textentry.bgcolor	"TransparentBlack"
		textentry.cursorcolor	"Orange"
		textentry.disabledtextcolor	"Orange"
		textentry.disabledbgcolor	"Blank"
		textentry.selectedtextcolor	"Black"
		textentry.selectedbgcolor	"Red"
		textentry.outoffocusselectedbgcolor	"Red"
		textentry.focusedgecolor	"TransparentBlack"
		togglebutton.selectedtextcolor	"Orange"
		tooltip.textcolor	"TransparentBlack"
		tooltip.bgcolor	"Red"
		treeview.bgcolor	"TransparentBlack"
		wizardsubpanel.bgcolor	"Blank"
		fgcolor	"Orange"
		bgcolor	"TransparentBlack"
		viewportbg	"Blank"
		team0	"204 204 204 255"
		team1	"255 64 64 255"
		team2	"153 204 255 255"
		mapdescriptiontext	"Orange"
		ct_blue	"153 204 255 255"
		t_red	"255 64 64 255"
		hostage_yellow	"Panel.FgColor"
		hudicon_green	"0 160 0 255"
		hudicon_red	"160 0 0 255"
		itemcolor	"255 167 42 200"
		menucolor	"233 208 173 255"
		menuboxbg	"0 0 0 100"
		selectionnumberfg	"255 220 0 200"
		selectiontextfg	"255 220 0 200"
		selectionemptyboxbg	"0 0 0 80"
		selectionboxbg	"0 0 0 80"
		selectionselectedboxbg	"0 0 0 190"
		hintmessagefg	"255 255 255 255"
		hintmessagebg	"0 0 0 60"
		progressbarfg	"255 30 13 255"
		main.title1.x	"32"
		main.title1.y	"180"
		main.title1.color	"255 255 255 255"
		main.title2.x	"380"
		main.title2.y	"205"
		main.title2.color	"255 255 255 80"
		main.title3.x	"460"
		main.title3.y	"-10"
		main.title3.color	"255 255 0 255"
		main.menu.x	"32"
		main.menu.y	"248"
		main.bottomborder	"32"
	}
	fonts
	{
		debugfixed
		{
			1
			{
				name	"Courier New"
				tall	"14"
				weight	"400"
				antialias	"1"
			}
		}
		debugfixedsmall
		{
			1
			{
				name	"Courier New"
				tall	"14"
				weight	"400"
				antialias	"1"
			}
		}
		default
		{
			1
			{
				name	"Verdana"
				tall	"9"
				weight	"700"
				antialias	"1"
				yres	"1 599"
			}
			2
			{
				name	"Verdana"
				tall	"12"
				weight	"700"
				antialias	"1"
				yres	"600 767"
			}
			3
			{
				name	"Verdana"
				tall	"14"
				weight	"900"
				antialias	"1"
				yres	"768 1023"
			}
			4
			{
				name	"Verdana"
				tall	"20"
				weight	"900"
				antialias	"1"
				yres	"1024 1199"
			}
			5
			{
				name	"Verdana"
				tall	"24"
				weight	"900"
				antialias	"1"
				yres	"1200 10000"
				additive	"1"
			}
		}
		defaultsmall
		{
			1
			{
				name	"Verdana"
				tall	"12"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"480 599"
			}
			2
			{
				name	"Verdana"
				tall	"13"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"600 767"
			}
			3
			{
				name	"Verdana"
				tall	"14"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"768 1023"
				antialias	"1"
			}
			4
			{
				name	"Verdana"
				tall	"20"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"1024 1199"
				antialias	"1"
			}
			5
			{
				name	"Verdana"
				tall	"24"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"1200 6000"
				antialias	"1"
			}
			6
			{
				name	"Arial"
				tall	"12"
				range	"0x0000 0x00FF"
				weight	"0"
			}
		}
		defaultverysmall
		{
			1
			{
				name	"Verdana"
				tall	"12"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"480 599"
			}
			2
			{
				name	"Verdana"
				tall	"13"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"600 767"
			}
			3
			{
				name	"Verdana"
				tall	"14"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"768 1023"
				antialias	"1"
			}
			4
			{
				name	"Verdana"
				tall	"20"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"1024 1199"
				antialias	"1"
			}
			5
			{
				name	"Verdana"
				tall	"24"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"1200 6000"
				antialias	"1"
			}
			6
			{
				name	"Verdana"
				tall	"12"
				range	"0x0000 0x00FF"
				weight	"0"
			}
			7
			{
				name	"Arial"
				tall	"11"
				range	"0x0000 0x00FF"
				weight	"0"
			}
		}
		menutitle
		{
			1
			{
				name	"Verdana Bold"
				tall	"18"
				weight	"500"
			}
		}
		weaponicons
		{
			1
			{
				name	"HalfLife2"
				tall	"64"
				weight	"0"
				antialias	"1"
				additive	"1"
				custom	"1"
			}
		}
		weaponiconsselected
		{
			1
			{
				name	"HalfLife2"
				tall	"64"
				weight	"0"
				antialias	"1"
				blur	"5"
				scanlines	"2"
				additive	"1"
				custom	"1"
			}
		}
		crosshairs
		{
			1
			{
				name	"HalfLife2"
				tall	"40"
				tall	"41"
				weight	"0"
				antialias	"0"
				additive	"1"
				custom	"1"
				yres	"1 10000"
			}
		}
		quickinfo
		{
			1
			{
				name	"HL2cross"
				tall	"28"
				tall	"50"
				weight	"0"
				antialias	"1"
				additive	"1"
				custom	"1"
			}
		}
		hudnumbers
		{
			1
			{
				name	"HalfLife2"
				tall	"32"
				weight	"0"
				antialias	"1"
				additive	"1"
				custom	"1"
			}
		}
		hudnumbersglow
		{
			1
			{
				name	"HalfLife2"
				tall	"32"
				weight	"0"
				blur	"4"
				scanlines	"2"
				antialias	"1"
				additive	"1"
				custom	"1"
			}
		}
		hudnumberssmall
		{
			1
			{
				name	"HalfLife2"
				name	"Helvetica Bold"
				tall	"16"
				weight	"1000"
				additive	"1"
				antialias	"1"
				custom	"1"
			}
		}
		hudselectionnumbers
		{
			1
			{
				name	"Verdana"
				tall	"11"
				weight	"700"
				antialias	"1"
				additive	"1"
			}
		}
		hudhinttextlarge
		{
			1
			{
				name	"Verdana"
				name	"Helvetica Bold"
				tall	"14"
				weight	"1000"
				antialias	"1"
				additive	"1"
			}
		}
		hudhinttextsmall
		{
			1
			{
				name	"Verdana"
				name	"Helvetica"
				tall	"11"
				weight	"0"
				antialias	"1"
				additive	"1"
			}
		}
		hudselectiontext
		{
			1
			{
				name	"Verdana"
				tall	"8"
				weight	"700"
				antialias	"1"
				yres	"1 599"
			}
			2
			{
				name	"Verdana"
				tall	"10"
				weight	"700"
				antialias	"1"
				yres	"600 767"
			}
			3
			{
				name	"Verdana"
				tall	"12"
				weight	"900"
				antialias	"1"
				yres	"768 1023"
			}
			4
			{
				name	"Verdana"
				tall	"16"
				weight	"900"
				antialias	"1"
				yres	"1024 1199"
			}
			5
			{
				name	"Verdana"
				tall	"17"
				weight	"1000"
				antialias	"1"
				yres	"1200 10000"
			}
		}
		budgetlabel
		{
			1
			{
				name	"Courier New"
				tall	"14"
				weight	"400"
				outline	"1"
			}
		}
		debugoverlay
		{
			1
			{
				name	"Courier New"
				tall	"14"
				weight	"400"
				outline	"1"
			}
		}
		closecaption_normal
		{
			1
			{
				name	"Tahoma"
				name	"Verdana"
				tall	"26"
				tall	"24"
				tall	"26"
				weight	"500"
			}
		}
		closecaption_italic
		{
			1
			{
				name	"Tahoma"
				name	"Verdana Italic"
				tall	"26"
				tall	"24"
				tall	"26"
				weight	"500"
				italic	"1"
			}
		}
		closecaption_bold
		{
			1
			{
				name	"Tahoma"
				name	"Verdana Bold"
				tall	"26"
				tall	"24"
				tall	"26"
				weight	"900"
			}
		}
		closecaption_bolditalic
		{
			1
			{
				name	"Tahoma"
				name	"Verdana Bold Italic"
				tall	"26"
				tall	"24"
				tall	"26"
				weight	"900"
				italic	"1"
			}
		}
		closecaption_small
		{
			1
			{
				name	"Tahoma"
				name	"Verdana"
				tall	"16"
				tall	"14"
				tall_hidef	"24"
				weight	"900"
				range	"0x0000 0x017F"
			}
		}
		marlett
		{
			1
			{
				name	"Marlett"
				tall	"14"
				weight	"0"
				symbol	"1"
			}
		}
		trebuchet24
		{
			1
			{
				name	"Trebuchet MS"
				tall	"24"
				weight	"900"
				range	"0x0000 0x007F"
				antialias	"1"
				additive	"1"
			}
		}
		trebuchet18
		{
			1
			{
				name	"Trebuchet MS"
				tall	"18"
				weight	"900"
			}
		}
		clienttitlefont
		{
			1
			{
				name	"HL2MP"
				tall	"46"
				weight	"0"
				additive	"0"
				antialias	"1"
			}
		}
		creditslogo
		{
			1
			{
				name	"HalfLife2"
				tall	"128"
				weight	"0"
				antialias	"1"
				additive	"1"
				custom	"1"
			}
		}
		creditstext
		{
			1
			{
				name	"Trebuchet MS"
				tall	"20"
				weight	"900"
				antialias	"1"
				additive	"1"
			}
		}
		creditsoutrologos
		{
			1
			{
				name	"HalfLife2"
				tall	"48"
				weight	"0"
				antialias	"1"
				additive	"1"
				custom	"1"
			}
		}
		creditsoutrotext
		{
			1
			{
				name	"Verdana"
				tall	"9"
				weight	"900"
				antialias	"1"
			}
		}
		centerprinttext
		{
			1
			{
				name	"Trebuchet MS"
				name	"Helvetica"
				tall	"18"
				weight	"900"
				antialias	"1"
				additive	"1"
			}
		}
		chatfont
		{
			1
			{
				name	"Verdana"
				tall	"12"
				weight	"700"
				yres	"480 599"
				dropshadow	"1"
			}
			2
			{
				name	"Verdana"
				tall	"13"
				weight	"700"
				yres	"600 767"
				dropshadow	"1"
			}
			3
			{
				name	"Verdana"
				tall	"14"
				weight	"700"
				yres	"768 1023"
				dropshadow	"1"
			}
			4
			{
				name	"Verdana"
				tall	"20"
				weight	"700"
				yres	"1024 1199"
				dropshadow	"1"
			}
			5
			{
				name	"Verdana"
				tall	"24"
				weight	"700"
				yres	"1200 10000"
				dropshadow	"1"
			}
		}
		targetid
		{
			1
			{
				name	"Trebuchet MS"
				tall	"24"
				weight	"900"
				range	"0x0000 0x007F"
				antialias	"1"
				additive	"0"
			}
		}
		hl2mptypedeath
		{
			1
			{
				name	"HL2MP"
				tall	"32"
				weight	"0"
				additive	"1"
				antialias	"1"
				custom	"1"
			}
		}
		defaultverysmallfallback
		{
			1
			{
				name	"Verdana"
				tall	"10"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"480 599"
				antialias	"1"
			}
			2
			{
				name	"Verdana"
				tall	"12"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"600 1199"
				antialias	"1"
			}
			3
			{
				name	"Verdana"
				tall	"15"
				weight	"0"
				range	"0x0000 0x017F"
				yres	"1200 6000"
				antialias	"1"
			}
		}
	}
	borders
	{
		baseborder
		{
			inset	"0 0 1 1"
			left
			{
				1
				{
					color	"Border.Dark"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Bright"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
		}
		titlebuttonborder
		{
			inset	"0 0 1 1"
			left
			{
				1
				{
					color	"Border.Bright"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Dark"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
		}
		titlebuttondisabledborder
		{
			inset	"0 0 1 1"
			left
			{
				1
				{
					color	"BgColor"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"BgColor"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"BgColor"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"BgColor"
					offset	"0 0"
				}
			}
		}
		titlebuttondepressedborder
		{
			inset	"1 1 1 1"
			left
			{
				1
				{
					color	"Border.Dark"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Bright"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
		}
		scrollbarbuttonborder
		{
			inset	"1 0 0 0"
			left
			{
				1
				{
					color	"Border.Bright"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Dark"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
		}
		scrollbarbuttondepressedborder
		{
			inset	"2 2 0 0"
			left
			{
				1
				{
					color	"Border.Dark"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Bright"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
		}
		buttonborder
		{
			inset	"0 0 0 0"
			left
			{
				1
				{
					color	"Border.Bright"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Bright"
					offset	"1 1"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
		}
		frameborder
		{
			inset	"0 0 1 1"
			left
			{
				1
				{
					color	"ControlBG"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"ControlBG"
					offset	"0 0"
				}
			}
			top
			{
				1
				{
					color	"ControlBG"
					offset	"0 1"
				}
			}
			bottom
			{
				1
				{
					color	"ControlBG"
					offset	"0 0"
				}
			}
		}
		tabborder
		{
			inset	"0 0 1 1"
			left
			{
				1
				{
					color	"Border.Bright"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Dark"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
		}
		tabactiveborder
		{
			inset	"0 0 1 0"
			left
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
			right
			{
				1
				{
					color	"Border.Dark"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"ControlBG"
					offset	"6 2"
				}
			}
		}
		tooltipborder
		{
			inset	"0 0 1 0"
			left
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			right
			{
				1
				{
					color	"Border.Dark"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
		}
		buttonkeyfocusborder
		{
			inset	"0 0 0 0"
			left
			{
				1
				{
					color	"Border.Bright"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Bright"
					offset	"1 1"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
		}
		buttondepressedborder
		{
			inset	"0 0 0 0"
			left
			{
				1
				{
					color	"Border.Bright"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Bright"
					offset	"1 1"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
		}
		comboboxborder
		{
			inset	"0 0 1 1"
			left
			{
				1
				{
					color	"Border.Dark"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Bright"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
		}
		menuborder
		{
			inset	"1 1 1 1"
			left
			{
				1
				{
					color	"Border.Bright"
					offset	"0 1"
				}
			}
			right
			{
				1
				{
					color	"Border.Dark"
					offset	"1 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
		}
		browserborder
		{
			inset	"0 0 0 0"
			left
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			right
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
			top
			{
				1
				{
					color	"Border.Dark"
					offset	"0 0"
				}
			}
			bottom
			{
				1
				{
					color	"Border.Bright"
					offset	"0 0"
				}
			}
		}
	}
	customfontfiles
	{
		1	"resource/HALFLIFE2.ttf"
		1	"resource/HL2MP.ttf"
		2	"resource/HL2crosshairs.ttf"
	}
}
